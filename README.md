# levar.co

levar.co is a working repository for storing live and test builds of levAR's main website.

This is the **deploy** branch.

Currently working on fixing spacing on index

## Authorship
**Contributors:**

Benjamin Chapman

Vikram Bhaduri

Daniel Esrig

Treytan White


## License
[CC BY-NC-ND](https://creativecommons.org/licenses/by-nc-nd/4.0/legalcode)
